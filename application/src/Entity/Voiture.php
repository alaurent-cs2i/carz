<?php

namespace App\Entity;

class Voiture
{
    private $marque;
    private $model;
    private $motorisation;
    private $prix;
    private $photo;

    public function __construct($marque, $model, $motorisation, $prix, $photo)
    {
        $this->marque  = $marque;
        $this->model   = $model;
        $this->motorisation  = $motorisation;
        $this->prix  = $prix;
        $this->photo  = $photo;

    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     * @return Voiture
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     * @return Voiture
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMotorisation()
    {
        return $this->motorisation;
    }

    /**
     * @param mixed $motorisation
     * @return Voiture
     */
    public function setMotorisation($motorisation)
    {
        $this->motorisation = $motorisation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     * @return Voiture
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return Voiture
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }


    public function toString()
    {
        return $this->marque." ".$this->model." ".$this->motorisation." ".$this->prix." ".$this->photo;
    }

}

?>