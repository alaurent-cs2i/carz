<?php

namespace App\Controller;

use App\Entity\Voiture;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function accueil()
    {
        return $this->render('Default/index.html.twig',
            [
            ]);
    }

    public function services()
    {
        return $this->render('Default/services.html.twig',
            [
            ]);
    }


    public function catalogue()
    {
        $v1 = new Voiture('Opel',      'Zafira Tourer',       '2.0 CDTI 110 Edition',                       '22800', 'opel-zafira-2015.jpg');
        $v2 = new Voiture('Mini',      'Countryman',          '2.0 SD 143 cooper pack red hot chili VBA',   '20000', 'mini-countryman-2016.jpg');
        $v3 = new Voiture('Toyota',    'Verso',               '1.3 VVT-i 100 STYLE',                        '15000', 'peugeot-207sw.jpg');
        $v4 = new Voiture('Peugeot',   '207 SW',              '1.4 VTI 95 Urban moove',                     '9690' , 'toyota-versos-2015.jpg');

        $voitures = [$v1, $v2, $v3, $v4];

        return $this->render('Default/catalogue.html.twig',
            [
                'voitures' => $voitures,
            ]);
    }

}
